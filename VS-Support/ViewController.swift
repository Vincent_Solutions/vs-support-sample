//
//  ViewController.swift
//  VS-Support
//
//  Created by Charles Eric Vincent on 2017-05-27.
//  Copyright © 2017 Vincent Solutions. All rights reserved.
//

import UIKit
import AWSMobileHubHelper

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        print("viewDidLoad")
        
        // Check if a user is logged in
        if !AWSSignInManager.sharedInstance().isLoggedIn {
            
            // If user is not logged in, present the sign in screen
            let loginStoryboard = UIStoryboard(name: "SignIn", bundle: nil)
            let loginController: SignInViewController = loginStoryboard.instantiateViewController(withIdentifier: "SignIn") as! SignInViewController
            
            // set canCancel property to false to require user to sign in before accessing the app
            // set canCancel to true to allow user to cancel the sign in process
            loginController.canCancel = false
            
            // assign the delegate for callback when user either signs in successfully or cancels sign in
            loginController.didCompleteSignIn = onSignIn
            
            // launch the sign in screen
            let navController = UINavigationController(rootViewController: loginController)
            navigationController?.present(navController, animated: true, completion: nil)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func onSignIn (_ success: Bool) {
        
        if (success) {
            // handle successful sign in
            // Perform operations like showing Welcome message
            DispatchQueue.main.async(execute: {
                let alert = UIAlertController(title: "Welcome",
                                              message: "Sign In Successful",
                                              preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion:nil)
            })
        } else {
            // handle cancel operation from user
        }
    }
    
    

}

