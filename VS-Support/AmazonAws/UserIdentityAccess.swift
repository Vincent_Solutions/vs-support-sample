//
//  UserIdentityAccess.swift
//  VS-Support
//
//  Created by Charles Eric Vincent on 2017-05-28.
//  Copyright © 2017 Vincent Solutions. All rights reserved.
//

import Foundation
import AWSMobileHubHelper

class UserIdentityAccess {
    
    func getUserIdentity() -> String {
        return AWSIdentityManager.default().identityId!
    }
}
